#include <pthread.h>
#include "kem.h"
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <x86intrin.h>
#include <openssl/evp.h>
#include <openssl/rsa.h>

pthread_t kyberThread, rsaThread;

static void * kyberTest(void *)
{

    uint8_t pk[CRYPTO_PUBLICKEYBYTES];
    uint8_t sk[CRYPTO_SECRETKEYBYTES];
    uint8_t ct[CRYPTO_CIPHERTEXTBYTES];
    uint8_t ss[CRYPTO_BYTES];

    uint64_t total_result = 0;
    uint64_t keygen_result = 0;
    uint64_t enc_result = 0;
    uint64_t dec_result = 0;
    const time_t itr = 50000;

    for(int i=0; i < itr; i++){
    uint64_t start = __rdtsc();

    if(crypto_kem_keypair(pk, sk) != 0){
        printf("Error: Keypair didnt generate correctly\n");
        return NULL;
    }

    uint64_t end = __rdtsc();
    keygen_result += (end - start);
    
    start = __rdtsc();

    if(crypto_kem_enc(ct, ss, pk) != 0)
    {
        printf("Error: Encryption Fault\n");
        return NULL;
    }

    end = __rdtsc();
    enc_result += (end - start);

    start = __rdtsc();

    if(crypto_kem_dec(ss,ct,sk) != 0)
    {
        printf("Error: Decryption Fault\n");
        return NULL;
    }

    end = __rdtsc();
    dec_result += (end - start);

    }

    total_result += (keygen_result + enc_result + dec_result);
    printf("\nKyber512:\nkeygen: %llu cycles/tick\nenc: %llu cycles/tick\ndec: %llu cycles/tick\ntotal: %llu cycles/tick\n\n", (keygen_result / itr), (enc_result / itr), (dec_result / itr), (total_result / itr));
    return 0;
}

static void* rsaTest(void *)
{
    uint64_t total_result = 0;
    uint64_t keygen_result = 0;
    uint64_t enc_result = 0;
    uint64_t dec_result = 0;
    const time_t itr = 20;
    const unsigned int modulus = 2048;
    EVP_PKEY_CTX *ctx = NULL;
    OSSL_LIB_CTX *ossl_ctx = NULL;
    size_t sslen = 0, ctlen = 0;
    unsigned char *ct = NULL, *ss = NULL;
    // create new openssl context
    ossl_ctx = OSSL_LIB_CTX_new();
    for (int i = 0; i < itr; i++)
    {
        uint64_t start = __rdtsc();
       
        // create a new key pair based on modulus
        EVP_PKEY *pkey = EVP_RSA_gen(modulus);

        uint64_t end = __rdtsc();
        keygen_result += (end - start);

        // create new context from the keys
        ctx = EVP_PKEY_CTX_new_from_pkey(ossl_ctx, pkey, NULL);
       
       
        // intiate the encapsulation
        EVP_PKEY_encapsulate_init(ctx, NULL);
       
        // set the kem operation
        EVP_PKEY_CTX_set_kem_op(ctx, "RSASVE");
       
        // determine buffer size for encapsulation parameters      
        EVP_PKEY_encapsulate(ctx, NULL, &ctlen, NULL, &sslen);

        // set memory based on buffer size
        ct = OPENSSL_malloc(ctlen);
        ss = OPENSSL_malloc(sslen);

        start = __rdtsc();

        // encapsulate
        EVP_PKEY_encapsulate(ctx, ct, &ctlen, ss, &sslen);

        end = __rdtsc();
        enc_result += (end - start);

        // decapsulate initiation
        EVP_PKEY_decapsulate_init(ctx, NULL);

        start = __rdtsc();

        // decapsulate
        EVP_PKEY_decapsulate(ctx, ss, &sslen, ct, ctlen);

        end = __rdtsc();
        dec_result += (end - start);

    }

    total_result += (keygen_result + enc_result + dec_result);
    printf("\nRSA2048:\nkeygen: %llu cycles/tick\nenc: %llu cycles/tick\ndec: %llu cycles/tick\ntotal: %llu cycles/tick\n\n", (keygen_result / itr), (enc_result / itr), (dec_result / itr), (total_result / itr));
    return 0;

    }

int main(void)
{
    pthread_create(&kyberThread, NULL, kyberTest, (void*)&kyberThread);
    pthread_create(&rsaThread, NULL, rsaTest, (void *)&rsaThread);
    pthread_join(kyberThread, NULL);
    pthread_join(rsaThread, NULL);
    return 0;
}
