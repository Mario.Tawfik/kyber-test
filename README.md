# Kyber
## Run Test
1. CD into /ref folder
2. Run `make all` command
3. Run `./test_time` command to run executable test

## Change number of total times each KEM is ran
1. CD into /ref folder
2. Open time_test.c file with preferred IDE or run `nano time_test.c` to edit
3. Change the i variable in the for loops to preferred amount
4. Press CTRL+X to save file if using nano
5. Run `make all` to re-compile executable

> **NOTE**! This package already includes all the [Kyber](https://github.com/pq-crystals/kyber) Project files.
